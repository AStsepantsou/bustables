import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BusTables {

	public static void main(String[] args) throws Exception {
			BufferedReader reader = new BufferedReader(new FileReader(args[0]));
			String line;
			List<String> posh = new ArrayList<String>();
			List<String> grotty = new ArrayList<String>();
			while ((line = reader.readLine()) != null) {
				if (line.contains("P"))
					posh.add(line);
				else
					grotty.add(line);
			}
			reader.close();

			Collections.sort(posh);
			Collections.sort(grotty);

			BufferedWriter writer = new BufferedWriter(new FileWriter("output.txt", false));

			for (int i = 0; i < posh.size(); i++) {
				writer.write(posh.get(i));
				writer.newLine();
			}
			writer.newLine();
			for (int i = 0; i < grotty.size(); i++) {
				writer.write(grotty.get(i));
				writer.newLine();
			}

			writer.flush();
			writer.close();

	}

}
